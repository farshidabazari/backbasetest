package com.farshidabz.backbasetest.data

import androidx.annotation.Keep

@Keep
data class CityModel(
    var country: String?,
    var name: String?,
    var _id: Long,
    var coord: Coord?
) {
    constructor(city: CityModel) : this(city.country, city.name, city._id, city.coord)

    fun getTitle(): String {
        return "${name ?: "Unknown"}, ${country ?: "Unknown"}"
    }

    fun getCityCoordinate(): String {
        return "Lat: ${coord?.lat ?: "Unknown"}, Lon: ${coord?.lon ?: "Unknown"}"
    }
}

@Keep
data class Coord(var lon: Float, var lat: Float)