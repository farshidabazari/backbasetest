package com.farshidabz.backbasetest.data

import androidx.annotation.Keep

/**
 * Trie
 *
 * Custom Trie model to create a prefix tree from cities name's.
 *
 * */

@Keep
class Trie {
    private val root = Root()

    /**
     * insert
     * @param [city] is CityModel
     *
     * Insert new node into the trie
     */

    fun insert(city: CityModel) {
        if (city.name.isNullOrEmpty()) {
            return
        }

        val split = city.name!!.split(" ").toTypedArray()
        for (word in split) {
            val chars = word.toCharArray()
            var startFrom: Node = root
            for (aChar in chars) {
                val found: Node = insert(aChar.toLowerCase(), startFrom, city)
                startFrom = found
                startFrom.addSuggestionObject(city)
            }
        }
    }

    private fun insert(currentChar: Char, startFrom: Node, city: CityModel): Node {
        val node: Node? = startFrom.children[currentChar]
        if (node == null) {
            val newNode: Node = Node().apply {
                nameChar = currentChar.toLowerCase()
                cityModel = city
            }
            startFrom.children[currentChar] = newNode
            return newNode
        }
        return node
    }

    /**
     * findWordsStartingWith
     * @param [prefix] is String
     * @return a list of CityModel
     *
     * search for a city name according to the entire prefix
     * */
    fun findWordsStartingWith(prefix: String): List<CityModel>? {
        val chars = prefix.toCharArray()
        var current: Node = root
        for (currentChar in chars) {
            val node = current.children[currentChar.toLowerCase()] ?: return ArrayList()
            current = node
        }
        return current.getSuggestionObject()
    }
}