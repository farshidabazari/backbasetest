package com.farshidabz.backbasetest.data.source

import com.farshidabz.backbasetest.data.CityModel
import com.farshidabz.backbasetest.data.Result
import com.farshidabz.backbasetest.data.Trie

interface CitiesRepository {
    suspend fun getCities(): Result<List<CityModel>>
    suspend fun createCityTrie(data: List<CityModel>): Trie
    suspend fun searchForACity(trie: Trie, searchWord: String) : Result<List<CityModel>?>
}