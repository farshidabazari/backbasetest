package com.farshidabz.backbasetest.data

import androidx.annotation.Keep

@Keep open class Node {
    var nameChar: Char? = null
    var cityModel: CityModel? = null
    var children = LinkedHashMap<Char, Node>()
    private var suggestionObject = arrayListOf<CityModel>()

    open fun isRoot() = false

    fun getSuggestionObject(): List<CityModel>? {
        return suggestionObject
    }

    fun addSuggestionObject(cityModel: CityModel) {
        suggestionObject.add(CityModel(cityModel))
    }
}