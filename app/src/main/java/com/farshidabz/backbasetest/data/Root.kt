package com.farshidabz.backbasetest.data

import androidx.annotation.Keep

@Keep
class Root : Node() {
    override fun isRoot(): Boolean {
        return true
    }
}