package com.farshidabz.backbasetest.data.source

import com.farshidabz.backbasetest.data.CityModel
import com.farshidabz.backbasetest.data.Result
import com.farshidabz.backbasetest.data.Trie
import com.farshidabz.backbasetest.di.AppModule.CitiesLocalDataSource
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import javax.inject.Inject

class DefaultCitiesRepository @Inject constructor(
    @CitiesLocalDataSource private val citiesDataSource: CitiesDataSource,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : CitiesRepository {

    /**
     * getCities
     * @return Result<List<CityModel>>
     *
     * load cities and return a list of cities as a Result
     * */
    override suspend fun getCities(): Result<List<CityModel>> {
        return withContext(ioDispatcher) {
            when (val cities = citiesDataSource.getCities()) {
                is Result.Error -> {
                    return@withContext Result.Error(cities.exception)
                }
                is Result.Success -> {
                    return@withContext Result.Success(cities.data)
                }
                else -> throw IllegalStateException()
            }
        }
    }

    /**
     * createCityTrie
     * @param [data] is  List<CityModel>
     * @return Trie
     *
     * Create a trie from list of cities
     * */
    override suspend fun createCityTrie(data: List<CityModel>): Trie {
        return withContext(ioDispatcher) {
            val trie = Trie()

            for (city in data) {
                city.name?.let { trie.insert(city) }
            }

            return@withContext trie
        }
    }

    override suspend fun searchForACity(trie: Trie, searchWord: String): Result<List<CityModel>?> {
        return withContext(ioDispatcher) {
            val foundList =
                trie.findWordsStartingWith(searchWord) ?: return@withContext Result.Error(null)
            return@withContext Result.Success(foundList)
        }
    }
}