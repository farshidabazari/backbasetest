package com.farshidabz.backbasetest.data.source.local

import android.content.Context
import com.farshidabz.backbasetest.R
import com.farshidabz.backbasetest.data.CityModel
import com.farshidabz.backbasetest.data.Result
import com.farshidabz.backbasetest.data.Result.Success
import com.farshidabz.backbasetest.data.source.CitiesDataSource
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class CitiesLocalDataSource internal constructor(
    private val context: Context,
    private val gson: Gson,
    private val ioDispatcher: CoroutineDispatcher = Dispatchers.IO
) : CitiesDataSource {
    /**
     * getCities
     *
     * @return Result<List<CityModel>>
     *
     * load cities json from resources, deserialize it as List<CityModel>
     * sort the list by the city and country name
     * */
    override suspend fun getCities(): Result<List<CityModel>> = withContext(ioDispatcher) {
        try {
            val json = context.resources.openRawResource(R.raw.cities)
                .bufferedReader().use { it.readText() }

            val cities =
                gson.fromJson<List<CityModel>>(json, object : TypeToken<List<CityModel>>() {}.type)

            val sortedList = cities.sortedWith(compareBy(CityModel::name, CityModel::country))

            return@withContext Success(sortedList)

        } catch (e: Exception) {
            return@withContext Result.Error(e)
        }
    }
}