package com.farshidabz.backbasetest.data.source

import com.farshidabz.backbasetest.data.CityModel
import com.farshidabz.backbasetest.data.Result

interface CitiesDataSource {
    suspend fun getCities(): Result<List<CityModel>>
}