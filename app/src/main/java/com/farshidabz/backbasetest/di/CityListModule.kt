package com.farshidabz.backbasetest.di

import androidx.lifecycle.ViewModel
import com.farshidabz.backbasetest.view.citylist.CityListFragment
import com.farshidabz.backbasetest.view.citylist.CityListViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

/**
 * Dagger module for the CityListFragment.
 */
@Module
abstract class CityListModule {
    @ContributesAndroidInjector(modules = [ViewModelBuilder::class])
    internal abstract fun cityListFragment(): CityListFragment

    @Binds
    @IntoMap
    @ViewModelKey(CityListViewModel::class)
    abstract fun bindViewModel(viewModel: CityListViewModel): ViewModel
}