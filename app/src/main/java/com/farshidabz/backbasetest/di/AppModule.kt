package com.farshidabz.backbasetest.di

import android.content.Context
import com.farshidabz.backbasetest.BackBaseApplication
import com.farshidabz.backbasetest.data.source.CitiesDataSource
import com.farshidabz.backbasetest.data.source.CitiesRepository
import com.farshidabz.backbasetest.data.source.DefaultCitiesRepository
import com.farshidabz.backbasetest.data.source.local.CitiesLocalDataSource
import com.google.gson.Gson
import dagger.Binds
import dagger.Module
import dagger.Provides
import kotlinx.coroutines.CoroutineDispatcher
import kotlinx.coroutines.Dispatchers
import javax.inject.Qualifier
import javax.inject.Singleton

@Module(includes = [ApplicationModuleBinds::class])
object AppModule {

    @Qualifier
    @Retention(AnnotationRetention.RUNTIME)
    annotation class CitiesLocalDataSource

    @JvmStatic
    @Singleton
    @CitiesLocalDataSource
    @Provides
    fun provideTasksLocalDataSource(
        context: Context,
        gson: Gson,
        ioDispatcher: CoroutineDispatcher
    ): CitiesDataSource {
        return CitiesLocalDataSource(context.applicationContext, gson, ioDispatcher)
    }

    @JvmStatic
    @Singleton
    @Provides
    fun provideIoDispatcher() = Dispatchers.IO

    @JvmStatic
    @Singleton
    @Provides
    fun provideGson() = Gson().newBuilder().create()
}

@Module
abstract class ApplicationModuleBinds {
    @Singleton
    @Binds
    abstract fun bindRepository(repo: DefaultCitiesRepository): CitiesRepository
}
