package com.farshidabz.backbasetest.di

import androidx.lifecycle.ViewModel
import com.farshidabz.backbasetest.view.map.MapFragment
import com.farshidabz.backbasetest.view.map.MapViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
abstract class MapModule {
    @ContributesAndroidInjector(modules = [ViewModelBuilder::class])
    internal abstract fun mapFragment(): MapFragment

    @Binds
    @IntoMap
    @ViewModelKey(MapViewModel::class)
    abstract fun bindViewModel(viewModel: MapViewModel): ViewModel
}