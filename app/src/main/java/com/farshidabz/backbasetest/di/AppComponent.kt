package com.farshidabz.backbasetest.di

import android.content.Context
import com.farshidabz.backbasetest.BackBaseApplication
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AndroidSupportInjectionModule::class,
        AppModule::class,
        ViewModelBuilder::class,
        CityListModule::class,
        MapModule::class]
)
interface AppComponent : AndroidInjector<BackBaseApplication> {
    @Component.Factory
    interface Factory {
        fun create(@BindsInstance applicationContext: Context): AppComponent
    }
}

