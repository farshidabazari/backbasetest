package com.farshidabz.backbasetest.view.citylist

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.farshidabz.backbasetest.data.CityModel
import com.farshidabz.backbasetest.data.Result.Error
import com.farshidabz.backbasetest.data.Result.Success
import com.farshidabz.backbasetest.data.Trie
import com.farshidabz.backbasetest.data.source.CitiesRepository
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * CityListViewModel
 * */

class CityListViewModel @Inject constructor(private val citiesRepository: CitiesRepository) :
    ViewModel() {
    var cityListView: CityListView? = null

    private val cities = MutableLiveData<List<CityModel>>().apply { value = emptyList() }

    private var searchText: String? = ""

    val items = MutableLiveData<List<CityModel>>().apply { value = emptyList() }

    val isSearchMode = MutableLiveData<Boolean>().apply { value = false }

    lateinit var cityTrie: Trie

    /**
     * load cities just one when initializing viewModel
     * */
    init {
        loadCities()
    }

    fun onCityClicked(cityModel: CityModel) {
        cityListView?.onCityClicked(cityModel)
    }

    fun onSearchButtonClicked() {
        isSearchMode.value = true
    }

    fun onClearOrCloseClicked() {
        cityListView?.onClearOrCloseClicked()
    }

    fun onSearchTextChanged(text: CharSequence) {
        searchText = text.toString()

        if (text.isEmpty()) {
            items.value = cities.value
        } else {
            isSearchMode.value = true
            searchForACity(text.toString())
        }
    }

    /**
     * loadCities
     *
     * read cities and fill the [cities] by the result data
     * then update the ui
     * then create a trie for search
     * */
    private fun loadCities() {
        viewModelScope.launch {
            when (val result = citiesRepository.getCities()) {
                is Success -> {
                    cities.value = result.data
                    items.value = cities.value
                    cityTrie = citiesRepository.createCityTrie(result.data)
                }

                is Error -> {
                }
            }
        }
    }

    /**
     * searchForACity
     *
     * @param [searchWord] is String
     *
     * search for a city by city name
     * */
    private fun searchForACity(searchWord: String) {
        if (::cityTrie.isInitialized) {
            viewModelScope.launch {
                when (val result = citiesRepository.searchForACity(cityTrie, searchWord)) {
                    is Success -> {
                        result.data?.let {
                            items.value = result.data
                        }
                    }
                    is Error -> {
                    }
                }
            }
        }
    }
}