package com.farshidabz.backbasetest.view.map

import androidx.lifecycle.ViewModel
import javax.inject.Inject

class MapViewModel @Inject constructor() : ViewModel() {
    var cityName : String = ""
    var country : String = ""
    var lat : Double = 0.0
    var lon : Double = 0.0

    fun getTitle(): String {
        return "$cityName, $country"
    }
}