package com.farshidabz.backbasetest.view.citylist.adapter

import androidx.databinding.BindingAdapter
import androidx.recyclerview.widget.RecyclerView
import com.farshidabz.backbasetest.data.CityModel

@BindingAdapter("app:items")
fun setItems(listView: RecyclerView, items: List<CityModel>?) {
    (listView.adapter as CityAdapter).submitList(null)
    (listView.adapter as CityAdapter).submitList(items?.toMutableList())
}