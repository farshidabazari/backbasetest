package com.farshidabz.backbasetest.view.citylist.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.farshidabz.backbasetest.data.CityModel
import com.farshidabz.backbasetest.databinding.ItemCityBinding
import com.farshidabz.backbasetest.view.citylist.CityListViewModel

class CityAdapter(private val viewModel: CityListViewModel) :
    ListAdapter<CityModel, CityViewHolder>(CityDiffCallback()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CityViewHolder {
        return CityViewHolder.from(parent)
    }

    override fun onBindViewHolder(holder: CityViewHolder, position: Int) {
        val item = getItem(position)

        holder.bind(viewModel, item)
    }
}

class CityViewHolder(val binding: ItemCityBinding) : RecyclerView.ViewHolder(binding.root) {
    fun bind(viewModel: CityListViewModel, item: CityModel) {

        binding.viewModel = viewModel
        binding.item = item
        binding.executePendingBindings()
    }

    companion object {
        fun from(parent: ViewGroup): CityViewHolder {
            val layoutInflater = LayoutInflater.from(parent.context)
            val binding = ItemCityBinding.inflate(layoutInflater, parent, false)

            return CityViewHolder(binding)
        }
    }
}

class CityDiffCallback : DiffUtil.ItemCallback<CityModel>() {
    override fun areItemsTheSame(oldItem: CityModel, newItem: CityModel): Boolean {
        return oldItem._id == newItem._id
    }

    override fun areContentsTheSame(oldItem: CityModel, newItem: CityModel): Boolean {
        return oldItem == newItem
    }
}
