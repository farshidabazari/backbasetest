package com.farshidabz.backbasetest.view.citylist

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.viewModels
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.observe
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import com.farshidabz.backbasetest.R
import com.farshidabz.backbasetest.data.CityModel
import com.farshidabz.backbasetest.databinding.FragmentCityListBinding
import com.farshidabz.backbasetest.util.clearKeyboard
import com.farshidabz.backbasetest.util.showKeyboard
import com.farshidabz.backbasetest.view.citylist.adapter.CityAdapter
import dagger.android.support.DaggerFragment
import javax.inject.Inject

class CityListFragment : DaggerFragment(), CityListView {
    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    lateinit var binding: FragmentCityListBinding
    private val viewModel by viewModels<CityListViewModel> { viewModelFactory }

    private lateinit var listAdapter: CityAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_city_list, container, false)
        return binding.root
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        configureViewModel()
        configureViews()
    }

    private fun configureViews() {
        binding.lifecycleOwner = viewLifecycleOwner
        binding.viewModel = viewModel

        setupRecyclerView()
    }

    private fun setupRecyclerView() {
        listAdapter = CityAdapter(viewModel)
        binding.cityListRecyclerView.apply {
            this.adapter = listAdapter
            addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }
    }

    private fun configureViewModel() {
        viewModel.cityListView = this

        viewModel.isSearchMode.observe(viewLifecycleOwner) {
            if (it) {
                binding.searchEditText.requestFocusFromTouch()
                showKeyboard()
            } else {
                clearKeyboard()
            }
        }
    }

    /**
     * onClearOrCloseClicked
     *
     * clear search bar if there are some texts
     * close the search bar is there isn't any texts to search
     * */
    override fun onClearOrCloseClicked() {
        if (binding.searchEditText.text.isNullOrEmpty()) {
            viewModel.isSearchMode.value = false
        } else {
            binding.searchEditText.setText("")
        }
    }

    /**
     * onCityClicked
     * @param [cityModel] is CityModel
     *
     * Navigate to map fragment while user clicks on a city
     *
     * */
    override fun onCityClicked(cityModel: CityModel) {
        val action = CityListFragmentDirections.actionCityListFragmentToMapFragment(
            cityModel.name ?: "",
            cityModel.coord?.lat ?: 0.0f,
            cityModel.coord?.lon ?: 0.0f,
            cityModel.country ?: ""
        )

        clearKeyboard()
        findNavController().navigate(action)
    }
}
