package com.farshidabz.backbasetest.view.citylist

import com.farshidabz.backbasetest.data.CityModel

interface CityListView {
    fun onClearOrCloseClicked()
    fun onCityClicked(cityModel: CityModel)
}