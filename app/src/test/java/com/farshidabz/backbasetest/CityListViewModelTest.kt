package com.farshidabz.backbasetest

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.farshidabz.backbasetest.data.CityModel
import com.farshidabz.backbasetest.data.Result
import com.farshidabz.backbasetest.data.Trie
import com.farshidabz.backbasetest.data.source.CitiesDataSource
import com.farshidabz.backbasetest.data.source.DefaultCitiesRepository
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.newSingleThreadContext
import kotlinx.coroutines.runBlocking
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.setMain
import org.hamcrest.CoreMatchers
import org.junit.*
import org.mockito.Mockito.mock
import kotlin.random.Random

class CityListViewModelTest {
    @get:Rule
    val instantTaskExecutorRule = InstantTaskExecutorRule()
    private val mainThreadSurrogate = newSingleThreadContext("UI thread")

    private val data = arrayListOf<CityModel>()
    private var trie = Trie()

    private var citiesDataSource = mock(CitiesDataSource::class.java)
    private var defaultRepository = DefaultCitiesRepository(citiesDataSource, Dispatchers.IO)

    @Before
    fun setUp() {
        Dispatchers.setMain(mainThreadSurrogate)

        data.add(CityModel("US", "Alabama", Random.nextLong(), null))
        data.add(CityModel("US", "Albuquerque", Random.nextLong(), null))
        data.add(CityModel("US", "Anaheim", Random.nextLong(), null))
        data.add(CityModel("IR", "Tehran", Random.nextLong(), null))
        data.add(CityModel("IR", "Tabriz", Random.nextLong(), null))
        data.add(CityModel("IR", "Takestan", Random.nextLong(), null))
        data.add(CityModel("AU", "Sydney", Random.nextLong(), null))

        createTrie()
    }

    @ExperimentalCoroutinesApi
    @After
    fun tearDown() {
        Dispatchers.resetMain()
        mainThreadSurrogate.close()
    }

    @Test
    fun `get search result and check result`() {
        runBlocking {
            when (val result = defaultRepository.searchForACity(trie, "Ta")) {
                is Result.Success -> {
                    val cities = result.data
                    Assert.assertThat(cities, CoreMatchers.`is`(CoreMatchers.notNullValue()))
                    Assert.assertThat(cities?.size, CoreMatchers.`is`(2))
                }
            }
        }
    }

    @Test
    fun `search with upper case name`() {
        runBlocking {
            when (val result = defaultRepository.searchForACity(trie, "Tehran")) {
                is Result.Success -> {
                    val cities = result.data
                    Assert.assertThat(cities, CoreMatchers.`is`(CoreMatchers.notNullValue()))
                    Assert.assertThat(cities?.size, CoreMatchers.`is`(1))
                    Assert.assertThat(cities?.get(0)?.name, CoreMatchers.`is`("Tehran"))
                    Assert.assertThat(cities?.get(0)?.country, CoreMatchers.`is`("IR"))
                }
            }
        }
    }

    @Test
    fun `search with lower case name`() {
        runBlocking {
            when (val result = defaultRepository.searchForACity(trie, "tehran")) {
                is Result.Success -> {
                    val cities = result.data
                    Assert.assertThat(cities, CoreMatchers.`is`(CoreMatchers.notNullValue()))
                    Assert.assertThat(cities?.size, CoreMatchers.`is`(1))
                    Assert.assertThat(cities?.get(0)?.name, CoreMatchers.`is`("Tehran"))
                    Assert.assertThat(cities?.get(0)?.country, CoreMatchers.`is`("IR"))
                }
                is Result.Error -> {
                    Assert.assertThat(125, CoreMatchers.equalTo(5))
                }
            }
        }
    }

    @Test
    fun `search relatively similar cities`() {
        runBlocking {
            when (val result = defaultRepository.searchForACity(trie, "al")) {
                is Result.Success -> {
                    val cities = result.data
                    Assert.assertThat(cities, CoreMatchers.`is`(CoreMatchers.notNullValue()))
                    Assert.assertThat(cities?.size, CoreMatchers.`is`(2))
                }
                is Result.Error -> {
                    Assert.assertThat(125, CoreMatchers.equalTo(5))
                }
            }
        }
    }

    @Test
    fun `search and check sorting`() {
        runBlocking {
            when (val result = defaultRepository.searchForACity(trie, "Ta")) {
                is Result.Success -> {
                    val cities = result.data
                    Assert.assertThat(cities, CoreMatchers.`is`(CoreMatchers.notNullValue()))
                    Assert.assertThat(cities?.size, CoreMatchers.`is`(2))
                    Assert.assertThat(cities?.get(0)?.name, CoreMatchers.`is`("Tabriz"))
                    Assert.assertThat(cities?.get(1)?.name, CoreMatchers.`is`("Takestan"))
                }
            }
        }
    }

    @Test
    fun `search cities with a char`() {
        runBlocking {
            when (val result = defaultRepository.searchForACity(trie, "a")) {
                is Result.Success -> {
                    val cities = result.data
                    Assert.assertThat(cities, CoreMatchers.`is`(CoreMatchers.notNullValue()))
                    Assert.assertThat(cities?.size, CoreMatchers.`is`(3))
                }
            }
        }
    }

    @Test
    fun `search cities with a undefined name`() {
        runBlocking {
            when (val result = defaultRepository.searchForACity(trie, "Shanghai")) {
                is Result.Success -> {
                    val cities = result.data
                    Assert.assertThat(cities, CoreMatchers.`is`(CoreMatchers.notNullValue()))
                    Assert.assertThat(cities?.size, CoreMatchers.`is`(0))
                }

                else -> {
                    Assert.assertThat(125, CoreMatchers.equalTo(5))
                }
            }
        }
    }

    @Test
    fun `search with invalid name`() {
        runBlocking {
            when (val result = defaultRepository.searchForACity(trie, "")) {
                is Result.Success -> {
                    val cities = result.data
                    Assert.assertThat(cities, CoreMatchers.`is`(CoreMatchers.notNullValue()))
                    Assert.assertThat(cities?.size, CoreMatchers.`is`(0))
                }
            }
        }
    }

    private fun createTrie() {
        runBlocking {
            trie = defaultRepository.createCityTrie(data)
        }
    }
}