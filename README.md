# BACKBASE skill test

<br>
this is an assignment from BACKBASE

### Tasks
- loading a big list of cities json 
- show loaded json into a list to the user 
- list should be sorted first by city name and then by country name
- allow orientation  
- search for a city with some conditions like: complexity should be lower than O(n)

### What did i do 
- load and show the json in to a RecyclerView
- sort list by city and country name
- search cities with O(c) complexity
- allow orientation 

### Technologies
- Dagger 2 for Dependency injection
- Mockito for mocking objects in test
- Kotlin as programming language 
- Navigation component for navigation between fragments       

### Architecture 
- I've chosen clean architecture to separate the layers (View, ViewModel, Model, Data)

### Search algorithm 
There are many search and sort algorithm with different complexities
as the assignment mentioned the complexity of search should be lower than O(n)
so there is some algorithm like binary search with O(log n) complexity 
after searching more about that I found that using tree is more efficient for filtering and autocomplete 
I've learned that some useful and popular tools like Elasticsearch are using Tree to improving search complexity 
But there are some issues with the tree, most of them are binary tree and for some cases like this one 
we need a tree with multiple nodes and children. I've used a data type most known as prefix tree or 
digital tree or Trie. the searching algorithm in this data type works like a binary search means first
we look for a tree that root element is same as or first Char, then in that element we search for a 
children that key is like or second char, by doing this to the end of or prefix we can find an element 
that contains our search prefix. the complexity of this algorithm is O(W) that W is our prefix word 
length. the complexity is not related to the data size rather is depend to our search word. in some cases
we can say the complexity is constant in this algorithm.

### How to run the application 
Just clone it and hit the run in android studio.         
 
